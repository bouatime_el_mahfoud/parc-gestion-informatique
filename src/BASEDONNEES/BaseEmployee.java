
package BASEDONNEES;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import parc.clas.Memoire;
public class BaseEmployee {
    private Connection cn=null;
    private Statement st=null;
    //le constructeur est bon;
    public BaseEmployee(){
         try{
        cn=DriverManager.getConnection("jdbc:"+Memoire.dburl+"/"+Memoire.databasename,Memoire.username,Memoire.password);
        
        System.out.println("connexion etablie");
        String table="Employee";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(CIN VARCHAR(100) not NULL,nom VARCHAR(100) not NULL ,prenom VARCHAR(100) not NULL,sexe VARCHAR(100) not NULL,nBureau VARCHAR(100) DEFAULT 'Aucun',PRIMARY KEY(CIN),"+
                  "FOREIGN KEY(nBureau)  REFERENCES Bureau(nb) ON DELETE SET NULL)" ;
           st.executeUpdate(sql);
           System.out.println("table cree");
        }
        
    }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la connexion");
            e.printStackTrace();
           
        }
    }
    public ResultSet existlog(String cin){
        PreparedStatement pr;
        ResultSet rs=null;
         try {
             pr = cn.prepareStatement("select * from Employee where CIN= ?");
             pr.setString(1, cin);
             rs=pr.executeQuery();
             return rs;
         } catch (SQLException ex) {
             Logger.getLogger(BaseAdmin.class.getName()).log(Level.SEVERE, null, ex);
         }
         return rs;
    }
    public void createEmployee(String x,String y,String z,String a){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("insert into Employee value(?,?,?,?,null)");
            ps.setString(1, x);
            ps.setString(2, y);
            ps.setString(3, z);
            ps.setString(4, a);
            ps.executeUpdate();
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("l'employee est ajouté");
            alert.showAndWait();
        }
        catch(SQLException e){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("l'employee existe deja");
            alert.showAndWait();
        }
    }
    public void supprimeremployee(String cin){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("delete from Employee where CIN=?");
            ps.setString(1, cin);
            ps.executeUpdate();
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("l'employee est supprimé");
            alert.showAndWait();
        }
        catch(SQLException e){
             Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("l'employee que vous essayé de supprimer n'existe pas");
            alert.showAndWait();
        }
        
    }
     public ResultSet afficherEmployee(){
        ResultSet rs=null;
        
        try{
            st=cn.createStatement();
            rs=st.executeQuery("select * from Employee");
            return rs;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public boolean possedeBureau(String cin){
        String test;
        ResultSet rs;
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("select nBureau from Employee where cin=?");
            ps.setString(1, cin);
            rs=ps.executeQuery();
            if(rs.next()){
                test=rs.getString("nBureau");
                if(test==null){
                    return false;
                }
                else return true;
            }
            
        }
        catch(SQLException e){
            System.out.println("chof employee bureau");
            e.printStackTrace();
        }
        return true;
    }
    public void affecterBureau(String cin,String nb){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("UPDATE Employee set nBureau=? where CIN=?");
            ps.setString(1, nb);
            ps.setString(2, cin);
            ps.executeUpdate();
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
}
