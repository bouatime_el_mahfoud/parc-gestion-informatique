/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BASEDONNEES;
import java.sql.*;
import javafx.scene.control.Alert;
import parc.clas.Login;
import parc.clas.Memoire;
/**
 *
 * @author theblackme
 */
//tres bon travail
public class BaseLogin {
    private Connection cn=null;
    private Statement st=null;
    private PreparedStatement ps=null;
    //il reste d'inclure la jointure dans login mais plus tard
    public BaseLogin(){
          try{
        
        cn=DriverManager.getConnection("jdbc:"+Memoire.dburl+"/"+Memoire.databasename,Memoire.username,Memoire.password);
        System.out.println("connexion etablie");
        String table="Login";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){
            System.out.println(" la table "+table+" existe deja ,au travail");
        }
        else{
            st=cn.createStatement();
            String sql="CREATE TABLE "+table+" ( log VARCHAR(100) not NULL ,password VARCHAR(100) not NULL,cinemp VARCHAR(100) not NULL,PRIMARY KEY(log),"+
                  "FOREIGN KEY(cinEmp) REFERENCES Employee(CIN) ON DELETE CASCADE )";
            st.executeUpdate(sql);
            System.out.println("la bd LOGIN CREE Avec succées");
        }
    }
          catch(SQLException e){
             System.out.println("une erreur s'est produit");
              
          }
}
    public Login existlog(String pass,String log){
        Login L=new Login("Aucun","Aucun","Aucun");
        ResultSet rs;
        try{
          ps=cn.prepareStatement("select * from Login where log=? and password=?");  
          ps.setString(1, log);
          ps.setString(2, pass);
          rs=ps.executeQuery();
          while(rs.next()){
              L.setlog(rs.getString("log"));
              L.setpass(rs.getString("password"));
              L.setcin(rs.getString("cinemp"));
              return L;
          }
        }
        catch(SQLException e){
            
        }
        return L;
    }
    public void createLogin(String x,String y,String z){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("insert into login value(?,?,?)");
            ps.setString(1, x);
            ps.setString(2, y);
            ps.setString(3, z);
            ps.executeUpdate();
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("le login associé a cet employee est :"+x+",le password est :"+z);
            alert.showAndWait();
        }
        catch(SQLException e){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("ce employee possede deja un login");
            alert.showAndWait();
        }
    }
    public ResultSet existpass(String pass,String cin){
        PreparedStatement ps;
        ResultSet rs=null;
        try{
            ps=cn.prepareStatement("select * from Login where password=? AND cinemp=?");
            ps.setString(1, pass);
            ps.setString(2, cin);
            rs=ps.executeQuery();
            return rs;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public void changermptdepasse(String cin,String pass){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("UPDATE Login set password=? where cinemp=?");
            ps.setString(1, pass);
            ps.setString(2, cin);
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public ResultSet existlogin(String log,String cin){
        PreparedStatement ps;
        ResultSet rs=null;
        try{
            ps=cn.prepareStatement("select * from Login where log=? AND cinemp=?");
            ps.setString(1, log);
            ps.setString(2, cin);
            rs=ps.executeQuery();
            return rs;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
     public void changerlogin(String cin,String login){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("UPDATE Login set log=? where cinemp=?");
            ps.setString(1, login);
            ps.setString(2, cin);
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
       
    }


