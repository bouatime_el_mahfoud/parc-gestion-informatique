/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BASEDONNEES;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import parc.clas.Administrateur;
import parc.clas.Login;
import parc.clas.Memoire;

public class BaseAdmin {
    public Connection cn=null;
    private Statement st=null;
    private PreparedStatement ps=null;
    //le constructeur est bon;
    public BaseAdmin(){
         try{
       cn=DriverManager.getConnection("jdbc:"+Memoire.dburl+"/"+Memoire.databasename,Memoire.username,Memoire.password);
        
        System.out.println("connexion etablie");
        String table="Admin";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(login VARCHAR(100) not NULL,nom VARCHAR(100) not NULL ,prenom VARCHAR(100) not NULL,password VARCHAR(100) not NULL,PRIMARY KEY(login)"+
                  ")" ;
           st.executeUpdate(sql);
           System.out.println("table Administrateur cree");
        }
        
    }
        catch(SQLException e){
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("une erreur s'est produit lors de la connexion à la base de donnée,veuillez verifier les paramétres de la base de donnée. ");
            alert.showAndWait();
        }
    }
    public Administrateur existlog(String pass,String log){
        Administrateur a=new Administrateur("Aucun","Aucun","Aucun","Aucun");
        ResultSet rs;
        try{
          ps=cn.prepareStatement("select * from Admin where login=? and password=?");  
          ps.setString(1, log);
          ps.setString(2, pass);
          rs=ps.executeQuery();
          while(rs.next()){
              a.setlogin(rs.getString("login"));
              a.setpassword(rs.getString("password"));
              a.setnom(rs.getString("nom"));
              a.setprenom(rs.getString("prenom"));
              return a;
          }
        }
        catch(SQLException e){
            
        }
        return a;
    }
     public Administrateur existlog(String log){
        Administrateur a=new Administrateur("Aucun","Aucun","Aucun","Aucun");
        ResultSet rs;
        try{
          ps=cn.prepareStatement("select * from Admin where login=?");  
          ps.setString(1, log);
          
          rs=ps.executeQuery();
          while(rs.next()){
              a.setlogin(rs.getString("login"));
              a.setpassword(rs.getString("password"));
              a.setnom(rs.getString("nom"));
              a.setprenom(rs.getString("prenom"));
              return a;
          }
        }
        catch(SQLException e){
            
        }
        return a;
    }
        
        
        
                }



