
package BASEDONNEES;
import java.sql.*;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import parc.clas.Memoire;
public class BaseBureau {

    private Connection cn=null;
    private Statement st=null;
    //mache correctement
    public BaseBureau(){
        try{
       cn=DriverManager.getConnection("jdbc:"+Memoire.dburl+"/"+Memoire.databasename,Memoire.username,Memoire.password);
        
        System.out.println("connexion etablie");
        String table="Bureau";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(etage INTEGER not NULL,nb VARCHAR(100) not NULL,batiement VARCHAR(100) not Null,PRIMARY KEY(nb))";
           st.executeUpdate(sql);
           System.out.println("la bd Bureau est cree avec succée");
     
        }
        
    }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la connexion ");
            e.printStackTrace();
           
        }
    }
    
    public  ResultSet rechercheBureau(String x){
        PreparedStatement ps;
        ResultSet rs=null;
        try{
            ps=cn.prepareStatement("select * from Bureau where nb=?");
            ps.setString(1,x);
            rs=ps.executeQuery();
            
            return rs;
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public void ajouterBureau(String a,int b,String c){
        PreparedStatement pr;
        try{
            pr=cn.prepareStatement("insert into Bureau value(?,?,?)");
            pr.setString(2, a);
            pr.setInt(1, b);
            pr.setString(3, c);
            pr.executeUpdate();
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("bureau ajouté");
            alert.showAndWait();
            
        }
        catch(SQLException e){
             Alert alert=new Alert(Alert.AlertType.WARNING);
             alert.setHeaderText(null);
             alert.setContentText("le bureau que vous essayer d'ajouter existe deja");
             alert.showAndWait();
             
        }
    }
    public void supprimerBureau(String x){
        PreparedStatement ps;
        try{
           ps=cn.prepareStatement("delete from Bureau where nb=? ");
           ps.setString(1, x);
           ps.executeUpdate();
        }
        catch(SQLException e){
            Alert alert=new Alert(Alert.AlertType.WARNING);
             alert.setHeaderText(null);
             alert.setContentText("une erreur s'est produit lors de la suppression du bureau");
             alert.showAndWait();
             
        }
    }
    public ResultSet afficherBureau(){
        ResultSet rs=null;
        
        try{
            st=cn.createStatement();
            rs=st.executeQuery("select * from Bureau");
            return rs;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    
}
