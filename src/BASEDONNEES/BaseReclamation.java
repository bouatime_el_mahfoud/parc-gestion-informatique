/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BASEDONNEES;
import java.sql.*;
import parc.clas.Memoire;
public class BaseReclamation {
    private Connection cn=null;
    private Statement st=null;
    //le constructeur est bon;
    public BaseReclamation(){
         try{
        cn=DriverManager.getConnection("jdbc:"+Memoire.dburl+"/"+Memoire.databasename,Memoire.username,Memoire.password);
        
        System.out.println("connexion etablie");
        String table="Reclamation";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(cEMP VARCHAR(100) not NULL,nseriem VARCHAR(100) not NULL ,Description VARCHAR(1000) not NULL,id INTEGER not NULL AUTO_INCREMENT,PRIMARY KEY(id),FOREIGN KEY(cEMP) REFERENCES Employee(CIN) ON DELETE CASCADE, "+
                  "FOREIGN KEY(nseriem)  REFERENCES Machine(nSerie) ON DELETE CASCADE )" ;
           st.executeUpdate(sql);
           System.out.println("table cree");
        }
        
    }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la connexion"); 
            
        }
    }
    public ResultSet afficherReclamations(){
        ResultSet rs=null;
        
        try{
            st=cn.createStatement();
            rs=st.executeQuery("select * from Reclamation");
            return rs;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public ResultSet rechercheReclamation(String id){
        PreparedStatement pr;
        ResultSet rs=null;
         try {
             pr = cn.prepareStatement("select * from Reclamation where id= ?");
             pr.setString(1, id);
             rs=pr.executeQuery();
             return rs;
         } catch (SQLException ex) {
             System.out.println("une erreur s'est produit lors de la recherche de la machine");
             ex.printStackTrace();
         }
         return rs;
    }
    public void supprimerreclamation(int id){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("delete from Reclamation where id=?");
            ps.setInt(1, id);
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void ajouterReclamation(String cemp,String nserie,String description){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("insert into Reclamation value(?,?,?,null)");
            ps.setString(1, cemp);
            ps.setString(2, nserie);
            ps.setString(3, description);
            ps.executeUpdate();
           
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
}
