
package BASEDONNEES;

import java.sql.*;
import javafx.scene.control.Alert;
import parc.clas.Memoire;


public class BaseMachine {
    private Connection cn=null;
    private Statement st=null;
    
    public BaseMachine() {
        String table="Machine";
        
        try{
        
        cn=DriverManager.getConnection("jdbc:"+Memoire.dburl+"/"+Memoire.databasename,Memoire.username,Memoire.password);
        System.out.println("connexion etablie");
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){
            System.out.println(" la table "+table+" existe deja ,au travail");
        }
        else{
          st=cn.createStatement();
           String sql = "CREATE TABLE Machine " +
                   "(nSerie VARCHAR(100) not NULL , " +
                   " type VARCHAR(100) not NULL , " + 
                   " marque VARCHAR(100) not NULL, " + 
                   " modele VARCHAR(100) not NULL, "+
                   "cinEmp VARCHAR(100) DEFAULT 'Aucun',"+
                   "nbur VARCHAR(100) DEFAULT 'Aucun',"+
                   "PRIMARY KEY ( nSerie ),"+
                   "FOREIGN KEY(cinEmp) REFERENCES Employee(CIN) ON DELETE SET NULL,FOREIGN KEY(nbur) REFERENCES Bureau(nb) ON DELETE SET NULL"+
                   ")";
                    
           st.executeUpdate(sql);
          System.out.println("la table machine est cree est cree");
        }
    }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la connexion à la base de donnée machine");
           
        }
    }
    public ResultSet rechercheMachine(String nSerie){
        PreparedStatement pr;
        ResultSet rs=null;
         try {
             pr = cn.prepareStatement("select * from Machine where nSerie= ?");
             pr.setString(1, nSerie);
             rs=pr.executeQuery();
             return rs;
         } catch (SQLException ex) {
             System.out.println("une erreur s'est produit lors de la recherche de la machine");
             ex.printStackTrace();
         }
         return rs;
    }
    public void ajoutermachine(String a,String b,String c,String d){
        PreparedStatement pr;
        try{
            pr=cn.prepareStatement("insert into Machine value(?,?,?,?,null,null)");
            pr.setString(1, a);
            pr.setString(3, b);
            pr.setString(4, c);
            pr.setString(2, d);
            pr.executeUpdate();
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("la machine :"+a+" ,est ajoutée avec succée");
            alert.showAndWait();
            
        }
        catch(SQLException e){
            Alert alert=new Alert(Alert.AlertType.WARNING);
             alert.setHeaderText(null);
             alert.setContentText("la machine que vous essayer d'ajouter existe deja");
             alert.showAndWait();
             return;
        }
    }
    public void supprimermachine(String x){
        PreparedStatement ps;
        try{
           ps=cn.prepareStatement("delete from Machine where nSerie=? ");
           ps.setString(1, x);
           ps.executeUpdate();
        }
        catch(SQLException e){
            Alert alert=new Alert(Alert.AlertType.WARNING);
             alert.setHeaderText(null);
             alert.setContentText("une erreur s'est produit lors de la suppression de la machine");
             alert.showAndWait();
             
        }
    }
    public ResultSet afficherMachine(){
        ResultSet rs=null;
        
        try{
            st=cn.createStatement();
            rs=st.executeQuery("select * from Machine");
            return rs;
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public boolean machinelibre(String nserie){
        ResultSet rs;
        PreparedStatement ps;
        String test;
        try{
           ps=cn.prepareStatement("select cinEmp from Machine where nSerie=?");
           ps.setString(1, nserie);
           rs=ps.executeQuery();
           if(rs.next()){
               test=rs.getString("cinEmp");
               if(test==null){
                   return true;
               }
               else{
                   return false;
               }
           }
           
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return false;
       
    }
    public boolean employeetype(String cinemp,String type){
        ResultSet rs;
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("select type from Machine where cinEmp=? AND type=?");
            ps.setString(1, cinemp);
            ps.setString(2, type);
            rs=ps.executeQuery();
            if(rs.next()){
                return true;
            }
            else return false;
        }
        catch(SQLException e){
            System.out.println("chof partie dyal emp machine");
            e.printStackTrace();
        }
        return true;
    }
    public void affecteremployer(String cin,String nserie){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("UPDATE Machine set cinEmp=? where nSerie=? ");
            ps.setString(1, cin);
            ps.setString(2, nserie);
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public void affecterBureau(String nb,String nSerie){
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("UPDATE Machine set nbur=? where nSerie=? ");
            ps.setString(1, nb);
            ps.setString(2,nSerie);
            ps.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        
    }
    public ResultSet EmplyeeMaxhineSerie(String cin){
        PreparedStatement ps;
        ResultSet rs=null;
        try{
            ps=cn.prepareStatement("select nSerie from Machine where cinEmp=?");
            ps.setString(1, cin); 
            rs=ps.executeQuery();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public ResultSet afficherMachieinfos(String cin,String type){
        PreparedStatement ps;
        ResultSet rs=null;
        try{
            ps=cn.prepareStatement("select * from Machine where cinEmp=? AND type=?");
            ps.setString(1, cin);
            ps.setString(2, type);
            rs=ps.executeQuery();
            return rs;
            
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return rs;
    }
    public ResultSet getemployeetype(String cinemp,String type){
        ResultSet rs=null;
        PreparedStatement ps;
        try{
            ps=cn.prepareStatement("select nSerie from Machine where cinEmp=? AND type=?");
            ps.setString(1, cinemp);
            ps.setString(2, type);
            rs=ps.executeQuery();
            return rs;
        }
        catch(SQLException e){
            System.out.println("chof partie dyal emp machine");
            e.printStackTrace();
        }
        return rs;
    }
}
