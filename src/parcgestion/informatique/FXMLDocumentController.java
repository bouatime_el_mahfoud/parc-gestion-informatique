/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcgestion.informatique;

import BASEDONNEES.BaseAdmin;
import BASEDONNEES.BaseBureau;
import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseLogin;
import BASEDONNEES.BaseMachine;
import BASEDONNEES.BaseReclamation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import parc.clas.Administrateur;
import parc.clas.Login;
import parc.clas.Memoire;
import parc.clas.config;


/**
 *
 * @author theblackme
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXButton conneButton;
    @FXML
    private JFXButton motpassebutton;
    BaseLogin bl;
    BaseAdmin ba;
    BaseEmployee be;
    BaseReclamation br;
    BaseMachine bm;
    BaseBureau bu;
    @FXML
    private JFXButton parametresbutton;
    
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    public void initialize(URL url, ResourceBundle rb) {
       File f=new File("config.txt");
       if(f.exists()){
           try {
               chargeribasedonnee();
               
               bu=new BaseBureau();
               ba=new BaseAdmin(); 
               be=new BaseEmployee();
               bm=new BaseMachine();
               bl= new BaseLogin();
               br=new BaseReclamation();
               
              
           } catch (IOException ex) {
               Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
           }
           
       }
       else{
           Alert alert=new Alert(Alert.AlertType.WARNING);
           alert.setHeaderText(null);
           alert.setContentText("les paramétres de la connexion à la base de donée ne sont pas établie,veuillez saisir les paramétres de votre base de donée");
           alert.showAndWait();
           System.out.println(Memoire.dburl);
           System.out.println(Memoire.databasename);
           System.out.println(Memoire.username);
           System.out.println(Memoire.password);
           
           
       }
               
    }

    @FXML
    private void connexion(ActionEvent event) {
         bu=new BaseBureau();
         ba=new BaseAdmin(); 
         be=new BaseEmployee();
         bm=new BaseMachine();
         bl= new BaseLogin();
         br=new BaseReclamation();
        String pass=password.getText();
        String log=username.getText();
        Login l=bl.existlog(pass, log);
        Administrateur a=ba.existlog(pass, log);
        if((l.getlog().equals(log))&&(l.getpass().equals(pass))){
            try {
                if(be.existlog(l.getcin()).next()){
                    Memoire.employeecin=l.getcin();
                    System.out.println(Memoire.employeecin);
                    loadwindow("/parc/employee/FXML.fxml","menu employee",816,483);
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("vous etes connecté à l'espace employée");
                    alert.showAndWait();
                    Stage stage=(Stage)conneButton.getScene().getWindow();
                    stage.close();
                    
                }
                
            } catch (SQLException ex) {
               ex.printStackTrace();
               System.out.println("une erreur s'est produit");
            }
        }
        
        else if((a.getlog().equals(log))&&(a.getpassword().equals(pass))){
            Memoire.adminlog=log;
            System.out.println(Memoire.adminlog);
            loadwindow("/Administrateur/administrateurmenu.fxml","menu Administrateur",916,640);
            Alert alert=new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("vous etes connecté à l'espace Administrateur");
            alert.showAndWait();
            Stage stage=(Stage)conneButton.getScene().getWindow();
            stage.close();
            
        }
        else{
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("le mot de pass ou le login est incorrecte");
            alert.showAndWait();
            
         
        } 
        
        }
        
      
        
            
        
      
        
    

    @FXML
    private void oublie(ActionEvent event) {
       
    }
     public void loadwindow(String loc,String title,int a,int b){
        try{
            Parent parent=FXMLLoader.load(getClass().getResource(loc));
            Stage stage=new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setMaxWidth(a);
            stage.setMaxHeight(b);
            stage.setScene(new Scene(parent));
            stage.show();
        }
        catch(IOException e){
            System.out.println("une erreur s'est produit lors du chargement du menu");
            e.printStackTrace();
        }
    }

    @FXML
    private void parametres(ActionEvent event) {
        loadwindow("/ConfigDatabase/config.fxml","parametres de la base de donée",916,640);
    }
    private void chargeribasedonnee() throws IOException{
        config c;
        ObjectInputStream ois;
        try{
            ois = new ObjectInputStream(
              new BufferedInputStream(
                new FileInputStream(
                  new File("config.txt"))));
             try {
                 c=(config)ois.readObject();
                 Memoire.dburl=c.getDbUrl();
                 Memoire.databasename=c.getNomdb();
                 Memoire.username=c.getUser();
                 Memoire.password=c.getPassword();
        
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
	
      ois.close();
        	
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }     	
        }
    }
    


    

