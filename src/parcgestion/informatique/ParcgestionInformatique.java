/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parcgestion.informatique;


import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseMachine;
import BASEDONNEES.BaseReclamation;

import java.sql.SQLException;

import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



/**
 *
 * @author theblackme
 */
public class ParcgestionInformatique extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("menu connexion");
        stage.setMaxWidth(777);
        stage.setMaxHeight(489);
        stage.setResizable(true);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException { 

       launch(args);
        
  
    }
    
}
