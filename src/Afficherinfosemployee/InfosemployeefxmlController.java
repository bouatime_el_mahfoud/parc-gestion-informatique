/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Afficherinfosemployee;

import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseMachine;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import parc.clas.Memoire;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class InfosemployeefxmlController implements Initializable {

    @FXML
    private Text numeroserieecran;
    @FXML
    private Text marqueecran;
    @FXML
    private Text modeleecran;
    @FXML
    private Text numeroserietraceur;
    @FXML
    private Text marquetraceur;
    @FXML
    private Text modeletraceur;
    @FXML
    private Text numeroserieunitécentrale;
    @FXML
    private Text marqueuntécentrale;
    @FXML
    private Text modeleunitécentrale;
    @FXML
    private Text numeroserieimprimante;
    @FXML
    private Text marqueimprimante;
    @FXML
    private Text modeleinmprimante;
    private BaseMachine bm;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bm=new BaseMachine();
        load();
        
    }   
    public void load(){
        ResultSet rs;
        rs=bm.afficherMachieinfos(Memoire.employeecin, "IMPRIMANTE");
        try{
            if(rs.next()){
                numeroserieimprimante.setText(rs.getString("nSerie"));
                marqueimprimante.setText(rs.getString("marque"));
                modeleinmprimante.setText(rs.getString("modele"));
            }
            else{
                numeroserieimprimante.setText("aucune imprimante");
                marqueimprimante.setText("marque");
                modeleinmprimante.setText("modele");
            }
            rs=bm.afficherMachieinfos(Memoire.employeecin, "ECRAN");
            if(rs.next()){
                numeroserieecran.setText(rs.getString("nSerie"));
                marqueecran.setText(rs.getString("marque"));
                modeleecran.setText(rs.getString("modele"));
            }
            else{
                numeroserieecran.setText("Aucun Ecren");
                marqueecran.setText("marque");
                modeleecran.setText("modele");
            }
            rs=bm.afficherMachieinfos(Memoire.employeecin, "unité centrale");
            if(rs.next()){
                numeroserieunitécentrale.setText(rs.getString("nSerie"));
                marqueuntécentrale.setText(rs.getString("marque"));
                modeleunitécentrale.setText(rs.getString("modele"));
            }
            else{
                numeroserieunitécentrale.setText("Aucune unité centrale");
                marqueuntécentrale.setText("marque");
                modeleunitécentrale.setText("modele");
            }
            rs=bm.afficherMachieinfos(Memoire.employeecin, "traceur");
             if(rs.next()){
                numeroserietraceur.setText(rs.getString("nSerie"));
                marquetraceur.setText(rs.getString("marque"));
                modeletraceur.setText(rs.getString("modele"));
            }
             else{
                numeroserietraceur.setText("Aucun traceur");
                marquetraceur.setText("marque");
                modeletraceur.setText("modele");
             }
            
        
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        

    
    }
    
}
