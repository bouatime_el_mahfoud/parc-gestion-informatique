/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionemployee;

import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseLogin;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class GestionemployeeController implements Initializable {

    @FXML
    private JFXTextField nomajout;
    @FXML
    private JFXTextField prenomajout;
    @FXML
    private JFXTextField cinajout;
    @FXML
    private JFXTextField sexeajout;
    @FXML
    private JFXButton Ajouteremployeebutton;
    @FXML
    private JFXTextField cinsupprimer;
    @FXML
    private Text nomsupprimer;
    @FXML
    private Text prenomsupprimer;
    @FXML
    private Text sexesupprimer;
    @FXML
    private JFXButton supprimeremployeebutton;
    @FXML
    private JFXButton quitterbutton;
    private BaseLogin bl;
    private BaseEmployee be;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bl=new BaseLogin();
        be=new BaseEmployee();
    }    

    @FXML
    private void Ajouteremployee(ActionEvent event) {
        String nom=nomajout.getText();
        String prenom=prenomajout.getText();
        String sexe=sexeajout.getText();
        String cin=cinajout.getText();
        if((nom.isEmpty())||(prenom.isEmpty())||(cin.isEmpty())||(sexe.isEmpty())){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("un champ est vide");
            alert.showAndWait();
            
        }
        else{
            be.createEmployee(cin, nom, prenom, sexe);
            bl.createLogin(nom+prenom+cin,cin, cin);
            
        }
        
    
   
    }

    @FXML
    private void supprimeremployee(ActionEvent event) {
        ResultSet rs;
        String cin=cinsupprimer.getText();
        try{
            if(cin.isEmpty()){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setHeaderText(null);
                alert.setContentText("veuilez saisir le cin de l'employee avant de cliquer le button supprimer");
                alert.showAndWait();
            }
            else{
                rs=be.existlog(cin);
                if(rs.next()){
                    be.supprimeremployee(cin);
                            
                }
                else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("l'employee n'existe pas pour le supprimer");
                    alert.showAndWait();
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void Quitter(ActionEvent event) {
        Stage stage=(Stage) quitterbutton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void cherchersupprime(ActionEvent event) {
        ResultSet rs;
        String cin=cinsupprimer.getText();
        try{
            rs=be.existlog(cin);
            if(rs.next()){
                nomsupprimer.setText(rs.getString("nom"));
                prenomsupprimer.setText(rs.getString("prenom"));
                sexesupprimer.setText(rs.getString("sexe"));
            }
            else{
                nomsupprimer.setText("aucun employee trouvé");
                prenomsupprimer.setText("prenom_employee");
                sexesupprimer.setText("sexe_employee");
            }
        }
        catch(SQLException e){
            System.out.println("une erreur s'est produit");
            e.printStackTrace();
        }
        
    }
    
}
