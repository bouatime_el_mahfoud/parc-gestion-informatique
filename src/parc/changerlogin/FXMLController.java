/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parc.changerlogin;

import BASEDONNEES.BaseLogin;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import parc.clas.Memoire;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class FXMLController implements Initializable {

    @FXML
    private JFXTextField ancienlogin;
    @FXML
    private JFXTextField nouveaulog1;
    @FXML
    private JFXTextField nouveaulog2;
    @FXML
    private JFXButton confirmerboutton;
    @FXML
    private JFXButton annulerbutton;
    private BaseLogin bl;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bl=new BaseLogin();
    }    

    @FXML
    private void confirmer(ActionEvent event) {
        String log= ancienlogin.getText();
        String log1=nouveaulog1.getText();
        String log2=nouveaulog2.getText();
        ResultSet rs;
        if(log.isEmpty()||log1.isEmpty()||log2.isEmpty()){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("un champ vide");
            alert.showAndWait();
        }
        else{
            rs=bl.existlogin(log, Memoire.employeecin);
            try{
                if(rs.next()){
                   if(log1.equals(log2)){
                    bl.changerlogin(Memoire.employeecin,log1);
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("login changé");
                    alert.showAndWait();
                }
                  else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("les nouveaux logins ne sont pas identiques");
                    alert.showAndWait();
                }
            }
             else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("votre login est incorrect");
                alert.showAndWait();
            }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            
           
        }
    }

    @FXML
    private void annuler(ActionEvent event) {
        Stage stage= (Stage) annulerbutton.getScene().getWindow();
        stage.close();
    }
    
}
