/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parc.employee;

import BASEDONNEES.BaseEmployee;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import parc.clas.Memoire;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class FXMLController implements Initializable {

    @FXML
    private MenuBar n;
    @FXML
    private JFXButton changerlogbutton;
    @FXML
    private JFXButton changerpassbutton;
    @FXML
    private JFXButton consultemachinebutton;
    @FXML
    private JFXButton reclamationbutton;
    @FXML
    private Text nomdatabase;
    @FXML
    private Text prenom_database;
    @FXML
    private Text n_bureau_Database;
    @FXML
    private JFXButton Quitterbutton;
    @FXML
    private JFXButton deconnexionbutton;
    private BaseEmployee be;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        be=new BaseEmployee();
        load();
    }    

    @FXML
    private void changerlog(ActionEvent event) {
       loadwindow("/parc/changerlogin/FXML.fxml","changer login",416,462);
    }

    @FXML
    private void changerpass(ActionEvent event) {
        loadwindow("/parc/changerpass/changerpass.fxml","changer mot de passe",416,464);
    }

    @FXML
    private void consultemachine(ActionEvent event) {
        loadwindow("/Afficherinfosemployee/infosemployeefxml.fxml","consulter vos machines",816,643);
    }

    @FXML
    private void reclamation(ActionEvent event) {
        loadwindow("/fairereclamation/fairereclamation.fxml","consulter vos machines",416,450);
    }

    @FXML
    private void Quitter(ActionEvent event) {
        Memoire.employeecin="";
        Stage stage = (Stage) Quitterbutton.getScene().getWindow();
       
       stage.close();
    }

    @FXML
    private void deconnexion(ActionEvent event) {
        Memoire.employeecin="";
        Stage stage = (Stage) deconnexionbutton.getScene().getWindow();
        loadwindow("/parcgestion/informatique/FXMLDocument.fxml","menu connexion",777,489);
       stage.close();
    }
    public void loadwindow(String loc,String title,int a,int b){
        try{
            Parent parent=FXMLLoader.load(getClass().getResource(loc));
            Stage stage=new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setMaxWidth(a);
            stage.setMaxHeight(b);
            stage.setScene(new Scene(parent));
            stage.show();
        }
        catch(IOException e){
            System.out.println("une erreur s'est produit lors du chargement du menu");
            e.printStackTrace();
        }
    }
    public void load(){
        ResultSet rs;
        try{
            rs=be.existlog(Memoire.employeecin);
            while(rs.next()){
                nomdatabase.setText(rs.getString("nom"));
                prenom_database.setText(rs.getString("prenom"));
                n_bureau_Database.setText(rs.getString("nBureau"));
            }
                    }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
}
