/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parc.changerpass;

import BASEDONNEES.BaseLogin;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import parc.clas.Memoire;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class ChangerpassController implements Initializable {

    @FXML
    private JFXPasswordField ancienpass;
    @FXML
    private JFXPasswordField nouveaupass1;
    @FXML
    private JFXPasswordField nouveaupass2;
    @FXML
    private JFXButton confirmerbutton;
    @FXML
    private JFXButton annulerbutton;
    private BaseLogin bl;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bl=new BaseLogin();
        
    }    

    @FXML
    private void confirmer(ActionEvent event) {
        String pass= ancienpass.getText();
        String pass1=nouveaupass1.getText();
        String pass2=nouveaupass2.getText();
        ResultSet rs;
        if(pass.isEmpty()||pass1.isEmpty()||pass2.isEmpty()){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("un champ vide");
            alert.showAndWait();
        }
        else{
            rs=bl.existpass(pass, Memoire.employeecin);
            try{
                if(rs.next()){
                   if(pass1.equals(pass2)){
                    bl.changermptdepasse(Memoire.employeecin,pass1);
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("mot de passe changé");
                    alert.showAndWait();
                }
                  else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("les nouveaux mots de pass ne sont pas identiques");
                    alert.showAndWait();
                }
            }
             else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("votre mot de pass est incorrect");
                alert.showAndWait();
            }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
            
           
        }
    }

    @FXML
    private void annuler(ActionEvent event) {
        Stage stage= (Stage) annulerbutton.getScene().getWindow();
        stage.close();
    }
    
    
}
