
package parc.clas;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Reclamation {
    private SimpleStringProperty cEmp;
    private SimpleStringProperty nSerie;
    private SimpleIntegerProperty id;
    public Reclamation(String x,String y,int a){
        id=new SimpleIntegerProperty(a);
        cEmp=new SimpleStringProperty(x);
        nSerie=new SimpleStringProperty(y);
        System.out.println("l'objet reclamation est cree avec succcée");
    }
    public String getCEmp(){
        return cEmp.get();
    }
    
    public int getId(){
        return id.get();
    }
    public String getNSerie(){
        return nSerie.get();
    }
    
  
   
   
}
