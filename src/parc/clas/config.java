/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parc.clas;

import java.io.Serializable;

/**
 *
 * @author theblackme
 */
public class config implements Serializable {
    private String dbUrl;
    private String user;
    private String password;
    private String nomdb;
    public config(String x,String y,String z,String a){
        dbUrl=x;
        user=y;
        password=z;
        nomdb=a;
    }
    public String getDbUrl(){
        return dbUrl;
    }
    public String getUser(){
        return user;
    }
    public String getPassword(){
        return password;
    }
    public String getNomdb(){
        return nomdb;
    }
}
