/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigDatabase;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import parc.clas.Memoire;
import parc.clas.config;


/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class ConfigController implements Initializable {

    @FXML
    private JFXTextField dburl;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXButton validerbutton;
    @FXML
    private JFXTextField nombd;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void valider(ActionEvent event) {
       String dburlv=dburl.getText();
       String usernamev=username.getText();
       String passwordv=password.getText();
       String namev=nombd.getText();
       if((dburlv.isEmpty())||(usernamev.isEmpty())||(passwordv.isEmpty())||(namev.isEmpty())){
           Alert alert=new Alert(Alert.AlertType.WARNING);
           alert.setHeaderText(null);
           alert.setContentText("un champ est vide");
           alert.showAndWait();
       }
       else{
           configurerBase(dburlv,usernamev,passwordv,namev);
           Memoire.dburl=dburlv;
           Memoire.username=usernamev;
           Memoire.password=passwordv;
           Memoire.databasename=namev;
           Alert alert=new Alert(Alert.AlertType.INFORMATION);
           alert.setHeaderText(null);
           alert.setContentText("les paramétres sont modifié");
           alert.showAndWait();
       }
    
    }
    private void configurerBase(String x,String y,String z,String a){
        ObjectInputStream ois;
    ObjectOutputStream oos;
    try {
      oos = new ObjectOutputStream(
              new BufferedOutputStream(
                new FileOutputStream(
                  new File("config.txt"))));
        	
      //Nous allons écrire chaque objet Game dans le fichier
     oos.writeObject(new config(x,y,z,a));
     System.out.println("done");
      oos.close();
     
        	
      //On récupère maintenant les données !
     
        	
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } 
    }
    
}