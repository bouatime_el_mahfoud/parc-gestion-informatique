
package fairereclamation;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
public class Reclamation extends Application {
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("fairereclamation.fxml"));
        
        Scene scene = new Scene(root);
        stage.setMaxWidth(416);
        stage.setMaxHeight(450);
        stage.setScene(scene);
        stage.setTitle("effectuer une reclamation");
        
        stage.setResizable(true);
        stage.show();
    }
    public static void main(String args[]){
        launch(args);
    }
}
