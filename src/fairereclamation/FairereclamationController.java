/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fairereclamation;

import BASEDONNEES.BaseMachine;
import BASEDONNEES.BaseReclamation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import parc.clas.Memoire;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class FairereclamationController implements Initializable {

    @FXML
    private JFXTextField typemachine;
    @FXML
    private JFXTextField descriptionreclamation;
    @FXML
    private JFXButton confirmerboutton;
    @FXML
    private JFXButton annulerbutton;
    private BaseReclamation br;
    private BaseMachine bm;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        br=new BaseReclamation();
        bm=new BaseMachine();
    }    

    @FXML
    private void confirmer(ActionEvent event) {
        String cin=Memoire.employeecin;
        String type=typemachine.getText();
        String desc=descriptionreclamation.getText();
        type=type.toUpperCase();
        ResultSet rs;
        if(cin.isEmpty()||type.isEmpty()||desc.isEmpty()){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("un champ vide");
            alert.showAndWait();
        }
        else{
           if((type.equals("IMPRIMANTE"))||(type.equals("TRACEUR"))||(type.equals("UNITÉ CENTRALE"))||(type.equals("ECRAN"))) {
               if(bm.employeetype(cin, type)){
                   rs=bm.getemployeetype(cin, type);
                   try{
                       if(rs.next()){
                       String nserie=rs.getString("nSerie");
                       br.ajouterReclamation(cin,nserie, desc);
                       Alert alert=new Alert(Alert.AlertType.WARNING);
                       alert.setHeaderText(null);
                       alert.setContentText("la reclamation est ajouté");
                       alert.showAndWait();
                                }
                       
                   }    
                   catch(SQLException e){
                       e.printStackTrace();
                   }
                   
               }
               else{
                   Alert alert=new Alert(Alert.AlertType.WARNING);
                   alert.setHeaderText(null);
                   alert.setContentText("vous ne possedez pas ce type de machine,consultez vos machines pour plus d'informations");
            
                   alert.showAndWait();
               }
               
           }
           else{
               Alert alert=new Alert(Alert.AlertType.WARNING);
               alert.setHeaderText(null);
               alert.setContentText("veuillezchoisir entre : Imprimante,Ecran,Unité centrale,Traceur");
            
               alert.showAndWait();
           }
          
        }
        
    }

    @FXML
    private void annuler(ActionEvent event) {
        Stage stage=(Stage) annulerbutton.getScene().getWindow();
        stage.close();
    }
    
}
