/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package afficherinfosadmin;

import BASEDONNEES.BaseBureau;
import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseMachine;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class InfosafminController implements Initializable {
    ObservableList<Employee> liste=FXCollections.observableArrayList();
    ObservableList<Machine> listm=FXCollections.observableArrayList();
    ObservableList<Bureau> listb=FXCollections.observableArrayList();
    @FXML
    private TableView<Employee> tableEmp;
    @FXML
    private TableColumn<Employee, String> cinCol;
    @FXML
    private TableColumn<Employee,String> nomCol;
    @FXML
    private TableColumn<Employee,String> prenomCol;
    @FXML
    private TableColumn<Employee,String> sexeCol;
    @FXML
    private TableColumn<Employee,String> numerob1Col;
    @FXML
    private TableView<Machine> tablemachine;
    @FXML
    private TableColumn<Machine, String> numerosCol;
    @FXML
    private TableColumn<Machine,String> typeCol;
    @FXML
    private TableColumn<Machine,String> marqueCol;
    @FXML
    private TableColumn<Machine,String> modeleCol;
     @FXML
    private TableColumn<Machine,String> numburCol;
    @FXML
    private TableColumn<Machine,String> cinempbCol;
    @FXML
    private TableView<Bureau> tableBureau;
    @FXML
    private TableColumn<Bureau,String> numerobCol;
    @FXML
    private TableColumn<Bureau, Integer> numeroeCol;
    @FXML
    private TableColumn<Bureau,String> batiementCol;
    @FXML
    private JFXButton quitterbutton;
    private BaseBureau bb;
    private BaseEmployee be;
    private BaseMachine bm;
   
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      be=new BaseEmployee();
      bb=new BaseBureau();
      bm=new BaseMachine();
      //partie Employee
      cinCol.setCellValueFactory(new PropertyValueFactory<>("cin"));
      nomCol.setCellValueFactory(new PropertyValueFactory<>("nom"));
      prenomCol.setCellValueFactory(new PropertyValueFactory<>("prenom"));
      sexeCol.setCellValueFactory(new PropertyValueFactory<>("sexe"));
      numerob1Col.setCellValueFactory(new PropertyValueFactory<>("nbureau"));
      loadEmployee();
      //partie Machine
      numerosCol.setCellValueFactory(new PropertyValueFactory<>("nSerie"));
      typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
      marqueCol.setCellValueFactory(new PropertyValueFactory<>("marque"));
      modeleCol.setCellValueFactory(new PropertyValueFactory<>("modele"));
      numburCol.setCellValueFactory(new PropertyValueFactory<>("nbureu"));
      cinempbCol.setCellValueFactory(new PropertyValueFactory<>("cinEmp"));
      loadMachine();
      //partie Bureau
      numerobCol.setCellValueFactory(new PropertyValueFactory<>("nB"));
      numeroeCol.setCellValueFactory(new PropertyValueFactory<>("etage"));
      batiementCol.setCellValueFactory(new PropertyValueFactory<>("battiement"));
      loadBureau();
      
      
    }    

    @FXML
    private void quitter(ActionEvent event) {
        Stage stage;
        stage = (Stage) quitterbutton.getScene().getWindow();
        stage.close();
    }
    public void loadBureau(){
        String z,x;
        int y;
        ResultSet rs=bb.afficherBureau();
        Bureau bur;
        try{
            while(rs.next()){
                x= rs.getString("nb");
                y=rs.getInt("etage");
                z=rs.getString("batiement");
                bur=new Bureau(x,y,z);
                listb.add(bur);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        tableBureau.setItems(listb);
    }
    //la classe Bureau
    
    public static class Bureau {
    private final SimpleStringProperty nB;
    private final   SimpleIntegerProperty etage;
    private final SimpleStringProperty battiement;
    private Bureau(String x,int y,String z){
        nB=new SimpleStringProperty(x);
        etage=new SimpleIntegerProperty(y);
       battiement=new SimpleStringProperty(z);
       System.out.println("un objet Bureau a été crée");
    }
    public Integer getEtage(){
        return etage.get();
    }
     public String getBattiement(){
        return battiement.get();
    }
    public String getNB(){
        return nB.get();
    }
    
}
    //la classe Employee
    public static class Employee {
    private final SimpleStringProperty cin;
    private final  SimpleStringProperty nom;
    private final  SimpleStringProperty prenom;
    private final SimpleStringProperty sexe;
    private final SimpleStringProperty nbureau;
    
    private Employee(String x,String y,String z,String a,String b){
        cin=new SimpleStringProperty(x);
        nom= new SimpleStringProperty(y);
        prenom=new SimpleStringProperty(z);
        sexe=new SimpleStringProperty(a);
        nbureau=new SimpleStringProperty(b);
        System.out.println("l'objet Employee est creer avec succée");
    }
    public String getNom(){
        return nom.get();
    }
    public String getPrenom(){
        return prenom.get();
    }
    public String getCin(){
        return cin.get();
    }
    public String getNbureau(){
        return nbureau.get();
    }
    public String getSexe(){
        return sexe.get();
    }
    
}
    public void loadEmployee(){
        String x,y,z,a,b;
        
        ResultSet rs=be.afficherEmployee();
        Employee emp;
        try{
            while(rs.next()){
                x= rs.getString("CIN");
                y=rs.getString("nom");
                z=rs.getString("prenom");
                a=rs.getString("sexe");
                b=rs.getString("nBureau");
                emp=new Employee(x,y,z,a,b);
                liste.add(emp);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        tableEmp.setItems(liste);
    }
    //la classe Machine
    public static class Machine {
    private final SimpleStringProperty nSerie;
    private final SimpleStringProperty type;
    private final SimpleStringProperty marque;
    private final SimpleStringProperty modele;
    private final SimpleStringProperty nbureu;
    private final SimpleStringProperty cinEmp;
    private Machine(String a,String b,String c,String d,String e,String f){
        nSerie=new SimpleStringProperty(a);
        type=new SimpleStringProperty(b);
        marque=new SimpleStringProperty(c);
        modele=new SimpleStringProperty(d);
        nbureu=new SimpleStringProperty(e);
        cinEmp=new SimpleStringProperty(f);
        
        System.out.println("l'objet machine est cree avec succée");
}
    public String getNSerie(){
        return nSerie.get();
    }
    public String getType(){
        return type.get();
    }
    public String getCinEmp(){
        return cinEmp.get();
    }
    public String getMarque(){
        return marque.get();
    }
    public String getModele(){
        return modele.get();
    }
    public String getNbureu(){
        return nbureu.get();
    }
  
}
    public void loadMachine(){
        String x,y,z,a,b,c;
        
        ResultSet rs=bm.afficherMachine();
        Machine mac;
        try{
            while(rs.next()){
                x= rs.getString("nSerie");
                y=rs.getString("type");
                z=rs.getString("marque");
                a=rs.getString("modele");
                b=rs.getString("cinEmp");
                c=rs.getString("nbur");
                mac=new Machine(x,y,z,a,c,b);
                listm.add(mac);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        tablemachine.setItems(listm);
    
    }
}
