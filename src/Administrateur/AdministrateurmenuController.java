/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Administrateur;

import BASEDONNEES.BaseAdmin;
import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseMachine;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import parc.clas.Memoire;
import parc.clas.Administrateur;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class AdministrateurmenuController implements Initializable {

    @FXML
    private Text nom_admin;
    @FXML
    private Text prenom_admin;
    @FXML
    private JFXButton deconnexionbutton;
    @FXML
    private JFXButton quitterbutton;
    @FXML
    private JFXButton gestionmachinebutton;
    @FXML
    private JFXButton gestionemployeebutton;
    @FXML
    private JFXButton gestionbureaubutton;
    @FXML
    private JFXTextField cinrecherche;
    @FXML
    private Text nomemployee;
    @FXML
    private Text prenomemployee;
    @FXML
    private Text typrmachine;
    @FXML
    private JFXTextField numeroserierecherch;
    @FXML
    private JFXButton informationsbutton;
    @FXML
    private JFXButton reclamationbutton;
    @FXML
    private JFXButton affectationbutton;
    private BaseEmployee be;
    private BaseAdmin ba;
    private BaseMachine bm;
    private ResultSet rs;
    private Statement st;
    private Administrateur ad;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ba=new BaseAdmin();
        be=new BaseEmployee();
        bm=new BaseMachine();
        ad=ba.existlog(Memoire.adminlog);
        Memoire.adminlog="";
        nom_admin.setText(ad.getnom());
        prenom_admin.setText(ad.getprenom());
        }
        
                
        
        
        
        

    @FXML
    private void deconnexion(ActionEvent event) {
       
       Stage stage = (Stage) deconnexionbutton.getScene().getWindow();
       loadwindow("/parcgestion/informatique/FXMLDocument.fxml","menu connexion",777,489);
       stage.close();
       
       
    }

    @FXML
    private void quitter(ActionEvent event) {
       
       Stage stage = (Stage) quitterbutton.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void gestionmachine(ActionEvent event) {
         loadwindow("/gestionmachines/gestionmachine.fxml","gestion des machines",949,750);
    }

    @FXML
    private void gestionemployee(ActionEvent event) {
        loadwindow("/gestionemployee/gestionemployee.fxml","gestion des employee",723,630);
    }
    

    @FXML
    private void gestionbureau(ActionEvent event) {
        loadwindow("/gestionbureau/gestionbureau.fxml","gestion des bureaux",841,682);
    }

    @FXML
    private void informations(ActionEvent event) {
       loadwindow("/afficherinfosadmin/infosafmin.fxml","consultation des informations",816,650);
    }

    @FXML
    private void reclamation(ActionEvent event) {
        loadwindow("/gestionreclamation/gestionreclamation.fxml","gestion des reclamations",959,801);
    }

    @FXML
    private void affectation(ActionEvent event) {
        loadwindow("/affectation/affectation.fxml","menu d'affectation",959,822);
    }
    public void loadwindow(String loc,String title,int a,int b){
        try{
            Parent parent=FXMLLoader.load(getClass().getResource(loc));
            Stage stage=new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setMaxWidth(a);
            stage.setMaxHeight(b);
            stage.setScene(new Scene(parent));
            stage.show();
        }
        catch(IOException e){
            System.out.println("une erreur s'est produit lors du chargement du menu");
            e.printStackTrace();
        }
    }

    @FXML
    private void chercheemployee(ActionEvent event) {
       
        rs=be.existlog(cinrecherche.getText());
        try{
            if(rs.next()){
            nomemployee.setText(rs.getString(2));
            prenomemployee.setText(rs.getString(3));
        }
            else{
                nomemployee.setText("aucun employee trouvé");
                prenomemployee.setText("prenom employee");
            }
            
        }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la recherche de l'employee");
            e.printStackTrace();
        }
        
        
    }

    @FXML
    private void cherchermachine(ActionEvent event) {
        rs=bm.rechercheMachine(numeroserierecherch.getText());
        try{
            if(rs.next()){
            typrmachine.setText(rs.getString(2));
            
        }
            else{
                typrmachine.setText("aucune machine trouvé");
                
            }
            
        }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la recherche de la machine");
            e.printStackTrace();
        }
    }
}
