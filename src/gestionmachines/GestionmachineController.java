/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionmachines;

import BASEDONNEES.BaseMachine;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class GestionmachineController implements Initializable {

    @FXML
    private JFXTextField numeroserieajout;
    @FXML
    private JFXTextField marqueajout;
    @FXML
    private JFXTextField modeleajout;
    @FXML
    private JFXTextField typeajout;
    @FXML
    private JFXButton ajoutermachinebutton;
    @FXML
    private JFXTextField numeroseriesupprimer;
    @FXML
    private Text marquesupprimer;
    @FXML
    private Text modelesupprimer;
    @FXML
    private Text typesupprimer;
    @FXML
    private JFXButton supprimermachinebutton;
    @FXML
    private JFXButton quitterbutton;
    private BaseMachine bm;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bm=new BaseMachine();
    }    

    @FXML
    private void ajoutermachine(ActionEvent event) {
        
        String numeros=numeroserieajout.getText();
        String marque=marqueajout.getText();
        String modele=modeleajout.getText();
        String type=typeajout.getText();
        type=type.toUpperCase();
        System.out.println(type);
        if((numeros.isEmpty())||(marque.isEmpty())||(modele.isEmpty())||(type.isEmpty())){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("un chmp est vide !");
            alert.showAndWait();
            
        }
        else{
            if((type.equals("IMPRIMANTE"))||(type.equals("TRACEUR"))||(type.equals("UNITÉ CENTRALE"))||(type.equals("ECRAN"))){
                bm.ajoutermachine(numeros,marque,modele,type);
               
                
            }
            else{
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setHeaderText(null);
                alert.setContentText("le type de machine que vous souhaité ajouté n'est pas permis,les type permis sont :IMPRIMANTE,TRACEUR,ECRAN,UNITÉ CENTRALE");
                alert.showAndWait();
                
            }
        }
            
    }

    @FXML
    private void supprimermachine(ActionEvent event) {
        String supp=numeroseriesupprimer.getText();
        
        if(!supp.isEmpty()){
            
            try {
                if(bm.rechercheMachine(supp).next()){
                    bm.supprimermachine(supp);
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("machine supprimé");
                    alert.showAndWait();
                    
                }
                else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("la machine que vous essayer de supprimé n'existe pas");
                    alert.showAndWait();
                    
                }
                    }
            catch (SQLException ex) {
                System.out.println("une erreur f chkel");
            }
        }
        else{
            Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("saisisez le numero de serie avant de cliquez le button supprimer");
                    alert.showAndWait();
                   
        }
    }

    @FXML
    private void quitter(ActionEvent event) {
        Stage stage=(Stage) quitterbutton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void supprimerRecherche(ActionEvent event) {
        ResultSet rs;
        String ns=numeroseriesupprimer.getText();
        try{
        rs=bm.rechercheMachine(ns);
        if(rs.next()){
            marquesupprimer.setText(rs.getString("marque"));
            modelesupprimer.setText(rs.getString("modele"));
            typesupprimer.setText(rs.getString("type"));
        }
        else{
            marquesupprimer.setText("aucune machine trouvé");
            modelesupprimer.setText("le modele");
            typesupprimer.setText("le type");
        }
          
    }
        catch(SQLException e){
            e.printStackTrace();
        }
        }
        
    
}
