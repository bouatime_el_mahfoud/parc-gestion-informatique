
package gestionbureau;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
public class GestionBureau extends Application{
     public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("gestionbureau.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("menu gestion bureau");
        stage.setMaxWidth(841);
        stage.setMaxHeight(682);
        stage.setResizable(true);
        stage.show();
    }
    public static void main(String args[]){
        launch(args);
    }
}
