/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionbureau;

import BASEDONNEES.BaseBureau;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class GestionbureauController implements Initializable {

    @FXML
    private JFXTextField numerobureuaajout;
    @FXML
    private JFXTextField numeroetageajout;
    @FXML
    private JFXTextField battiementajout;
    @FXML
    private JFXButton ajouterbureaubutton;
    @FXML
    private JFXTextField numerobureausupprimer;
    @FXML
    private Text numeroetagesupprimer;
    @FXML
    private Text battiementsupprimer;
    @FXML
    private JFXButton supprimerbureaubutton;
    @FXML
    private JFXButton quitterbutton;
    private BaseBureau bb;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        bb=new BaseBureau();
    }    

    @FXML
    private void ajouterbureau(ActionEvent event) {
        try{
            String nub=numerobureuaajout.getText();
            String etage=numeroetageajout.getText();
            String bat=battiementajout.getText();
            if((nub.isEmpty())||(etage.isEmpty())||(bat.isEmpty())){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setHeaderText(null);
                alert.setContentText("un champ est vide");
                alert.showAndWait();
                
            }
            else{
                int etage1=Integer.parseInt(numeroetageajout.getText());
                bb.ajouterBureau(nub, etage1, bat);
                
                
            }
        }
        catch(NumberFormatException e){
            Alert alert=new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("le champ etage doit uniquement contenir des nombres, pas de caractéres");
            alert.showAndWait();
            
        }
       
        
    }

    @FXML
    private void supprimerbureau(ActionEvent event) {
        String supp=numerobureausupprimer.getText();
        if(!supp.isEmpty()){
               try {
                if(bb.rechercheBureau(supp).next()){
                    bb.supprimerBureau(supp);
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("Bureau supprimé");
                    alert.showAndWait();
                    return;
                }
                else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("la Bureau que vous essayer de supprimr n'existe pas");
                    alert.showAndWait();
                    
                }
                    }
            catch (SQLException ex) {
                System.out.println("une erreur f chkel");
            }
        }
        else{
            Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("saisisez le numero de Bureau avant de cliquez le button supprimer");
                    alert.showAndWait();
                    
        
        }
    }

    @FXML
    private void quitter(ActionEvent event) {
       Stage stage=(Stage) quitterbutton.getScene().getWindow();
       stage.close();
    }

    @FXML
    private void cherchesupprimer(ActionEvent event) {
        ResultSet rs;
        String supp=numerobureausupprimer.getText();
        try{
            rs=bb.rechercheBureau(supp);
            if(rs.next()){
                numeroetagesupprimer.setText(Integer.toString(rs.getInt("etage")));
                battiementsupprimer.setText(rs.getString("batiement"));
                
            }
            else{
                numeroetagesupprimer.setText("aucun bureau trouvé");
                battiementsupprimer.setText("batiement");
            }
        }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de la recherche");
        }
        
    }
    
    
    
    
    
   
    
    
}
