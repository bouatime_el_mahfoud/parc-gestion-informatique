/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionreclamation;

import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseMachine;
import BASEDONNEES.BaseReclamation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class GestionreclamationController implements Initializable {
    ObservableList<Reclamation> listr=FXCollections.observableArrayList();
    @FXML
    private TableView<Reclamation> tablereclamation;
    @FXML
    private TableColumn<Reclamation,Integer> idreclamationcolonne;
    @FXML
    private TableColumn<Reclamation,String> numeroseriemachinecolonne;
    @FXML
    private TableColumn<Reclamation,String> cinemployeecolonne;
    @FXML
    private JFXTextField idreclamationgestion;
    @FXML
    private Text numeroseriemachingestionreclamation;
    @FXML
    private Text nomemployeereclamationgestion;
    @FXML
    private Text typedemachinereclamationgestion;
    @FXML
    private Text prenomemployeereclamationgestion;
    @FXML
    private Text numerodebureaureclamationgestion;
    @FXML
    private TextArea Descriptionreclamationgestion;
    @FXML
    private JFXButton validerreclamationbutton;
    @FXML
    private JFXButton quitterbutton;
    private BaseReclamation br;
    private BaseEmployee be;
    private BaseMachine bm;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bm=new BaseMachine();
        br=new BaseReclamation();
        be=new BaseEmployee();
        idreclamationcolonne.setCellValueFactory(new PropertyValueFactory<>("id"));
        numeroseriemachinecolonne.setCellValueFactory(new PropertyValueFactory<>("nSerie"));
        cinemployeecolonne.setCellValueFactory(new PropertyValueFactory<>("cEmp"));
        loadReclamation();
    }   
    public void loadReclamation(){
        String x,y;
        int z;
        ResultSet rs;
        try{
            rs=br.afficherReclamations();
            while(rs.next()){
                z=rs.getInt("id");
                y=rs.getString("cEMP");
                x=rs.getString("nseriem");
                Reclamation r=new Reclamation(y,x,z);
                listr.add(r);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        tablereclamation.setItems(listr);
    }

    @FXML
    private void validerreclamation(ActionEvent event) {
        ResultSet rs;
        String valid=idreclamationgestion.getText();
        if(valid.isEmpty()){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("veuillez saisir le numero de l' id avant de cliquer le button valider");
            alert.showAndWait();
        }
        else{
            try{
                rs=br.rechercheReclamation(valid);
                if(rs.next()){
                    br.supprimerreclamation(Integer.parseInt(valid));
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setContentText("la reclamation est validé");
                    alert.showAndWait();
                }
                else{
                    Alert alert =new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setContentText("la reclamation n'existe pas");
                    alert.showAndWait();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void quitter(ActionEvent event) {
        Stage stage= (Stage) quitterbutton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void chercherecla(ActionEvent event) {
        String idr=idreclamationgestion.getText();
        ResultSet rs;
        try{
            rs=br.rechercheReclamation(idr);
            if(rs.next()){
                String cinemp;
                String nser=rs.getString("nseriem");
                numeroseriemachingestionreclamation.setText(nser);
                Descriptionreclamationgestion.setText(rs.getString("Description"));
                cinemp=rs.getString("cEMP");
                rs=be.existlog(cinemp);
                if(rs.next()){
                    nomemployeereclamationgestion.setText(rs.getString("nom"));
                    
                    
                    if(rs.getString("nBureau")==null){
                        numerodebureaureclamationgestion.setText("aucun bureau");
                    }
                    else{
                        numerodebureaureclamationgestion.setText(rs.getString("nBureau"));
                    }
                    prenomemployeereclamationgestion.setText(rs.getString("prenom"));
                }
                else{
                    nomemployeereclamationgestion.setText("nom employee");
                    
                    numerodebureaureclamationgestion.setText("numero de bureau");
                    prenomemployeereclamationgestion.setText("prenom employee");
                }
                rs=bm.rechercheMachine(nser);
                if(rs.next()){
                    typedemachinereclamationgestion .setText(rs.getString("type"));
                }
                else{
                    typedemachinereclamationgestion.setText("type de la machine");
                }
                
            }
            else{
                numeroseriemachingestionreclamation.setText("aucune reclamation");
                nomemployeereclamationgestion.setText("nom employee");
                numerodebureaureclamationgestion.setText("numero de bureau");
                prenomemployeereclamationgestion.setText("prenom employee");
                typedemachinereclamationgestion.setText("type de la machine");
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    public static class Reclamation {
    private final SimpleStringProperty cEmp;
    private final SimpleStringProperty nSerie;
    private final SimpleIntegerProperty id;
    private Reclamation(String x,String y,int a){
        id=new SimpleIntegerProperty(a);
        cEmp=new SimpleStringProperty(x);
        nSerie=new SimpleStringProperty(y);
        System.out.println("l'objet reclamation est cree avec succcée");
    }
    public String getCEmp(){
        return cEmp.get();
    }
    
    public int getId(){
        return id.get();
    }
    public String getNSerie(){
        return nSerie.get();
    }
    
  
   
   
}

}
