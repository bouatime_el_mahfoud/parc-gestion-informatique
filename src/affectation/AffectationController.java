/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package affectation;

import BASEDONNEES.BaseBureau;
import BASEDONNEES.BaseEmployee;
import BASEDONNEES.BaseMachine;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author theblackme
 */
public class AffectationController implements Initializable {

    @FXML
    private JFXTextField numeroseriemachineaffecte;
    @FXML
    private Text typemachineaffecte;
    @FXML
    private Text marquemachineaffecte;
    @FXML
    private Text modelemachineaffecte;
    @FXML
    private JFXTextField cinemployeemachineaffecte;
    @FXML
    private Text nomemployeemachineaffecte;
    @FXML
    private Text prenomemployeemachineaffecte;
    @FXML
    private JFXButton affectermachineemployeebutton;
    @FXML
    private JFXTextField cinemployeebureauaffecte;
    @FXML
    private Text nomemployeebureauaffecte;
    @FXML
    private Text prenomemployeebureauaffecte;
    @FXML
    private JFXTextField numerobureauaffecte;
    @FXML
    private Text battimentaffecte;
    @FXML
    private Text numeroetageaffecte;
    @FXML
    private JFXButton affecteremployeebureaubutton;
    @FXML
    private JFXButton quitterbutton;
    //les bases de données
    private BaseEmployee be;
    private BaseBureau bb;
    private BaseMachine bm;
   
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        bm=new BaseMachine();
        bb=new BaseBureau();
        be=new BaseEmployee();
        
    }    

    @FXML
    private void affectermachineemployee(ActionEvent event) {
        ResultSet rs;
        String numeroserie= numeroseriemachineaffecte.getText();
        String cin=cinemployeemachineaffecte.getText();
        if(cin.isEmpty()||numeroserie.isEmpty()){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("veuillez saisir le numero de serie de la machine et le cin de l'employee avant d'appuyez le button affecter");
            alert.showAndWait();
        }
        else{
            rs=bm.rechercheMachine(numeroserie);
            try{
                if(rs.next()){
                    String type=rs.getString("type");
                if(bm.machinelibre(numeroserie)){
                    rs=be.existlog(cin);
                    if(rs.next()){
                        String nb=rs.getString("nBureau");
                        if(bm.employeetype(cin, type)){
                            Alert alert=new Alert(Alert.AlertType.WARNING);
                            alert.setHeaderText(null);
                            alert.setContentText("l'affectation n'est pas possible car l'utilisateur posséde deja une machine de ce type");
                            alert.showAndWait();
                        }
                        else{
                            bm.affecteremployer(cin, numeroserie);
                            Alert alert=new Alert(Alert.AlertType.INFORMATION);
                            alert.setHeaderText(null);
                            alert.setContentText("l'affectation est validé");
                            alert.showAndWait();
                            if(be.possedeBureau(cin)){
                                bm.affecterBureau(nb, numeroserie);
                                Alert alert1= new Alert(Alert.AlertType.INFORMATION);
                                alert1.setHeaderText(null);
                                alert1.setContentText("la machine est affecté automatiquement au bureau de l'employee");
                                alert1.showAndWait();
                            }
                            else{
                                Alert alert1=new Alert(Alert.AlertType.INFORMATION);
                                alert1.setHeaderText(null);
                                alert1.setContentText("la machine n'est pas affecté à un bureau car l'employee ne possede pas un bureau");
                                alert1.showAndWait();
                            }
                        }
                    }
                    else{
                        Alert alert=new Alert(Alert.AlertType.ERROR);
                        alert.setHeaderText(null);
                        alert.setContentText("l'employee n'existe pas");
                        alert.showAndWait();
                    }
                }
                else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setContentText("la machine que vous essayer d'affecter est deja affecté à un autre employee");
                    alert.showAndWait();
                }
                
            }
            else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setContentText("la machine que vous essayer d'affecter n'existe pas");
                alert.showAndWait();
            }
            }
            catch(SQLException e){
                System.out.println("erreur");
            }
           
            
        }
        
    }

    @FXML
    private void affecteremployeebureau(ActionEvent event) {
        ResultSet rs;
        String cin=cinemployeebureauaffecte.getText();
        String nb=numerobureauaffecte.getText();
        if(cin.isEmpty()||nb.isEmpty()){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setContentText("veuillez saisir le CIN de l'employee et le numero de Bureau avant d'appuyer le button affecter");
            alert.showAndWait();
        }
        else{
            try{
                rs=be.existlog(cin);
                if(rs.next()){
                    rs=bb.rechercheBureau(nb);
                    if(rs.next()){
                        if(be.possedeBureau(cin)){
                             Alert alert1=new Alert(Alert.AlertType.WARNING);
                             alert1.setHeaderText(null);
                             alert1.setContentText("l'employee possede deja un bureau");
                             alert1.showAndWait();
                        }
                        else{
                            be.affecterBureau(cin, nb);
                             Alert alert1=new Alert(Alert.AlertType.INFORMATION);
                             alert1.setHeaderText(null);
                             alert1.setContentText("l'affectation est valide");
                             alert1.showAndWait();
                             rs=bm.EmplyeeMaxhineSerie(cin);
                                boolean b=false;
                                 while(rs.next()){
                                     bm.affecterBureau(nb, rs.getString("nSerie"));
                                     b=true;
                                 }
                                 if(b==false){
                                     Alert alert=new Alert(Alert.AlertType.INFORMATION);
                                     alert.setHeaderText(null);
                                     alert.setContentText("Aucune machine n'est affecté au bureau,car l'employee ne possede aucune machine");
                                     alert.showAndWait();
                                 }
                                 else{
                                     Alert alert=new Alert(Alert.AlertType.INFORMATION);
                                 alert.setHeaderText(null);
                                 alert.setContentText("les machines de l'employee sont affecté automatiquement au Bureau");
                                 alert.showAndWait();
                                 }
                                 
                             
                            
                        }
                    }
                    else{
                        Alert alert1=new Alert(Alert.AlertType.ERROR);
                        alert1.setHeaderText(null);
                        alert1.setContentText("le bureau n'existe pas");
                        alert1.showAndWait();
                        
                    }
                }
                else{
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setContentText("l'employee n'existe pas");
                    alert.showAndWait();
                }
            }
            catch(SQLException e){
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void quitter(ActionEvent event) {
        Stage stage= (Stage) quitterbutton.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void numeroMachineaAffecte(ActionEvent event) {
        String nserie=numeroseriemachineaffecte.getText();
        ResultSet rs;
        try{
            rs=bm.rechercheMachine(nserie);
            if(rs.next()){
                typemachineaffecte.setText(rs.getString("type"));
                marquemachineaffecte.setText(rs.getString("marque"));
                modelemachineaffecte.setText(rs.getString("modele"));
            }
            else{
                typemachineaffecte.setText("aucune machine trouvée");
                marquemachineaffecte.setText("Marque");
                modelemachineaffecte.setText("Modele");
            }
        }
        catch(SQLException e){
            System.out.println("une erreur s'est produit lors de l'execution");
            e.printStackTrace();
            
        }
    }

    @FXML
    private void cinEmployeMachineaffecte(ActionEvent event) {
        String cin=cinemployeemachineaffecte.getText();
        ResultSet rs;
        try{
            rs=be.existlog(cin);
            if(rs.next()){
                nomemployeemachineaffecte.setText(rs.getString("nom"));
                prenomemployeemachineaffecte.setText(rs.getString("prenom"));
            }
            else{
                 nomemployeemachineaffecte.setText("aucun employee trouvé");
                 prenomemployeemachineaffecte.setText("prenom");
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void cinemployeebureau(ActionEvent event) {
         String cin=cinemployeebureauaffecte.getText();
        ResultSet rs;
        try{
            rs=be.existlog(cin);
            if(rs.next()){
                nomemployeebureauaffecte.setText(rs.getString("nom"));
                prenomemployeebureauaffecte.setText(rs.getString("prenom"));
            }
            else{
                 nomemployeebureauaffecte.setText("aucun employee trouvé");
                 prenomemployeebureauaffecte.setText("prenom");
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    @FXML
    private void numeroBureauAffecte(ActionEvent event) {
        String nb=numerobureauaffecte.getText();
        ResultSet rs;
        try{
            rs=bb.rechercheBureau(nb);
            if(rs.next()){
                battimentaffecte.setText(rs.getString("batiement"));
                numeroetageaffecte.setText(Integer.toString(rs.getInt("etage")));
            }
            else{
                battimentaffecte.setText("batiement");
                numeroetageaffecte.setText("aucun bureau trouvé");
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
}
